﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace assessment2
{
    public class MenuItem
    {
        private string desc;
        private double price;
        private bool isVeg;
       
        public string Desc
        {
            get
            {
                return desc;
            }
            set
            {
                desc = value;
            }
        }
        public double Price
        {
            get
            {
                return price;
            }
            set
            {
                price = value;
            }
        }

        public bool Vegetarian
        {
            get
            {
                return isVeg;
            }
            set
            {
                isVeg = value;
            }
        }

        //method used to convert value true or false to string Y or N
        public static string Veg(bool isVeg)
        {
            if (isVeg) { return "Y"; }

            return "N";
        }
       
        //Methos that return the string rapresentation of the menu item
        public override string ToString()
        {
            return Desc + Price + "\t" + Veg(Vegetarian);
        }

        

    }
} 
