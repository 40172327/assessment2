﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace assessment2
{
    /// <summary>
    /// Interaction logic for ManagerWindow.xaml
    /// </summary>
    public partial class ManagerWindow : Window
    {
        Restaurant newRest = new Restaurant();
        
        internal ManagerWindow(Restaurant rest)
        {
            InitializeComponent();

            //copy old restarunat to a new restaurant reference
            newRest = rest;

            //population of comboserver in manager window
            for (int i = 0; i < rest.CountServer(); i++)
            {
                comboEmps.Items.Add(rest.showServer(i));
            }
        }

        //Button items displays buttons that will act on the menu's items list
        private void btnItems_Click(object sender, RoutedEventArgs e)
        {
            lstMod.Visibility = Visibility.Visible;
            lstDriversMod.Visibility = Visibility.Hidden;
            lstServersMod.Visibility = Visibility.Hidden;

            lstMod.ItemsSource = newRest.completeMenu();


            btnAddItem.Visibility = Visibility.Visible;
            btnAmendItem.Visibility = Visibility.Visible;
            btnRemoveItem.Visibility = Visibility.Visible;
            btnAddServer.Visibility = Visibility.Hidden;
            btnAmendServer.Visibility = Visibility.Hidden;
            btnRemoveServer.Visibility = Visibility.Hidden;
            btnAddDriver.Visibility = Visibility.Hidden;
            btnAmendDriver.Visibility = Visibility.Hidden;
            btnRemoveDriver.Visibility = Visibility.Hidden;
            lblAddingServer.Visibility = Visibility.Hidden;
            lblServerName.Visibility = Visibility.Hidden;
            txtServerName.Visibility = Visibility.Hidden;
            lblServerId.Visibility = Visibility.Hidden;
            txtServerId.Visibility = Visibility.Hidden;
            btnAddToServer.Visibility = Visibility.Hidden;
            btnAmendToServer.Visibility = Visibility.Hidden;
            lblAmendingServer.Visibility = Visibility.Hidden;
            lblAddingDriver.Visibility = Visibility.Hidden;
            lblDriverName.Visibility = Visibility.Hidden;
            txtDriverName.Visibility = Visibility.Hidden;
            lblDriverId.Visibility = Visibility.Hidden;
            txtDriverId.Visibility = Visibility.Hidden;
            lblReg.Visibility = Visibility.Hidden;
            txtReg.Visibility = Visibility.Hidden;
            btnAddToDriver.Visibility = Visibility.Hidden;
            lblAmendingDriver.Visibility = Visibility.Hidden;
            btnAddToDriver.Visibility = Visibility.Hidden;
            btnAmendToDriver.Visibility = Visibility.Hidden;


        }
        //Button with display all fields related with adding an item to the menu
        private void btnAddItem_Click(object sender, RoutedEventArgs e)
        {

            lblAddingItem.Visibility = Visibility.Visible;
            btnAddToMenu.Visibility = Visibility.Visible;
            lblDesc.Visibility = Visibility.Visible;
            txtDesc.Visibility = Visibility.Visible;
            lblPrice.Visibility = Visibility.Visible;
            txtPrice.Visibility = Visibility.Visible;
            lblVeg.Visibility = Visibility.Visible;
            rdVegNo.Visibility = Visibility.Visible;
            rdVegYes.Visibility = Visibility.Visible;
            txtVeg.Visibility = Visibility.Hidden;
            lblAmendingItem.Visibility = Visibility.Hidden;
            btnAmendMenuItem.Visibility = Visibility.Hidden;
        }

        //Button with display all fields related with amending an item to the menu
        private void btnAmendItem_Click(object sender, RoutedEventArgs e)
        {
            if (lstMod.SelectedIndex == -1)
            {
                MessageBox.Show("Please select an Item first!");
            }
            else
            {
                lblAddingItem.Visibility = Visibility.Hidden;
                lblAmendingItem.Visibility = Visibility.Visible;
                lblDesc.Visibility = Visibility.Visible;
                txtDesc.Visibility = Visibility.Visible;
                lblPrice.Visibility = Visibility.Visible;
                txtPrice.Visibility = Visibility.Visible;
                lblVeg.Visibility = Visibility.Visible;
                txtVeg.Visibility = Visibility.Visible;
                rdVegNo.Visibility = Visibility.Hidden;
                rdVegYes.Visibility = Visibility.Hidden;
                btnAmendMenuItem.Visibility = Visibility.Visible;

                String menuit = lstMod.SelectedItem.ToString();
                string[] words = menuit.Split('\t');
                txtDesc.Text = words[0];
                txtPrice.Text = words[1];
                txtVeg.Text = words[2];
            }
        }

        //button adding item to menu
        private void btnAddToMenu_Click(object sender, RoutedEventArgs e)
        {


            MenuItem item = new MenuItem();
            string testDesc = txtDesc.Text;
            string testPrice = txtPrice.Text.ToString();
            //Regex expression to check for correct form of description acceppting letter and spaces but not numbers
            if (!System.Text.RegularExpressions.Regex.IsMatch(testDesc, "^[a-zA-Z ]+$"))
            {
                MessageBox.Show("Please enter a valid description");
                txtDesc.Focus();
                lblAddingItem.Visibility = Visibility.Visible;
                lblDesc.Visibility = Visibility.Visible;
                lblPrice.Visibility = Visibility.Visible;
                lblVeg.Visibility = Visibility.Visible;
                txtDesc.Visibility = Visibility.Visible;
                txtPrice.Visibility = Visibility.Visible;
                rdVegNo.Visibility = Visibility.Visible;
                rdVegYes.Visibility = Visibility.Visible;
                btnAddToMenu.Visibility = Visibility.Visible;
                txtDesc.Clear();
            }
            //Regex expression to check for correct form of price acceppting only numbers
            else if (!System.Text.RegularExpressions.Regex.IsMatch(testPrice, "^[0-9]+$"))
            {
                MessageBox.Show("Please enter a valid price");
                txtPrice.Focus();

                lblAddingItem.Visibility = Visibility.Visible;
                lblDesc.Visibility = Visibility.Visible;
                lblPrice.Visibility = Visibility.Visible;
                lblVeg.Visibility = Visibility.Visible;
                txtDesc.Visibility = Visibility.Visible;
                txtPrice.Visibility = Visibility.Visible;
                rdVegNo.Visibility = Visibility.Visible;
                rdVegYes.Visibility = Visibility.Visible;
                btnAddToMenu.Visibility = Visibility.Visible;
                txtPrice.Clear();
            }
            else if (rdVegNo.IsChecked == false && rdVegYes.IsChecked == false)
            {
                MessageBox.Show("Please select an option between vegetarina d anot vegetarian");
                lblAddingItem.Visibility = Visibility.Visible;
                lblDesc.Visibility = Visibility.Visible;
                lblPrice.Visibility = Visibility.Visible;
                lblVeg.Visibility = Visibility.Visible;
                txtDesc.Visibility = Visibility.Visible;
                txtPrice.Visibility = Visibility.Visible;
                rdVegNo.Visibility = Visibility.Visible;
                rdVegYes.Visibility = Visibility.Visible;
                btnAddToMenu.Visibility = Visibility.Visible;

            }

            else
            {
                item.Desc = txtDesc.Text + " \t $";
                item.Price = double.Parse(txtPrice.Text);
                if (rdVegNo.IsChecked == true)
                {
                    item.Vegetarian = true;
                }
                else
                {
                    item.Vegetarian = false;

                }
                lstMod.ItemsSource = null;
                newRest.addItemToMenu(item);
                lstMod.ItemsSource = newRest.completeMenu();
                rdVegNo.IsChecked = false;
                rdVegYes.IsChecked = false;
                txtDesc.Clear();
                txtPrice.Clear();

                lblAddingItem.Visibility = Visibility.Hidden;
                lblDesc.Visibility = Visibility.Hidden;
                lblPrice.Visibility = Visibility.Hidden;
                lblVeg.Visibility = Visibility.Hidden;
                txtDesc.Visibility = Visibility.Hidden;
                txtPrice.Visibility = Visibility.Hidden;
                rdVegNo.Visibility = Visibility.Hidden;
                rdVegYes.Visibility = Visibility.Hidden;
                btnAddToMenu.Visibility = Visibility.Hidden;
            }



        }

        //Button will remove an item from the menu
        private void btnRemoveItem_Click(object sender, RoutedEventArgs e)
        {
            newRest.removeItem(lstMod.SelectedIndex);
            lstMod.ItemsSource = null;
            lstMod.ItemsSource = newRest.completeMenu();
        }
        private void btnAmendMenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = new MenuItem();
            string testDesc = txtDesc.Text;
            string testPrice = txtPrice.Text.ToString();
            string testVeg = txtVeg.Text;
            if (!System.Text.RegularExpressions.Regex.IsMatch(testDesc, "^[a-zA-Z ]+$"))
            {
                MessageBox.Show("Please enter a valid description");
                txtDesc.Focus();
                lblAddingItem.Visibility = Visibility.Hidden;
                lblDesc.Visibility = Visibility.Visible;
                lblPrice.Visibility = Visibility.Visible;
                lblVeg.Visibility = Visibility.Visible;
                txtDesc.Visibility = Visibility.Visible;
                txtPrice.Visibility = Visibility.Visible;
                rdVegNo.Visibility = Visibility.Visible;
                rdVegYes.Visibility = Visibility.Visible;
                btnAddToMenu.Visibility = Visibility.Visible;
                txtDesc.Clear();
            }
            else if (!System.Text.RegularExpressions.Regex.IsMatch(testPrice, "[0-9]{0,2}.?[0-9]{0,2}$"))
            {
                MessageBox.Show("Please enter a valid price");
                txtPrice.Focus();

                lblAddingItem.Visibility = Visibility.Hidden;
                lblDesc.Visibility = Visibility.Visible;
                lblPrice.Visibility = Visibility.Visible;
                lblVeg.Visibility = Visibility.Visible;
                txtDesc.Visibility = Visibility.Visible;
                txtPrice.Visibility = Visibility.Visible;
                rdVegNo.Visibility = Visibility.Visible;
                rdVegYes.Visibility = Visibility.Visible;
                btnAddToMenu.Visibility = Visibility.Visible;
                txtPrice.Clear();
            }
            else if (!System.Text.RegularExpressions.Regex.IsMatch(testVeg, "^[YN]+$"))
            {
                MessageBox.Show("Enter Y for vegetarian or N for not vegetarian");
                lblAddingItem.Visibility = Visibility.Hidden;
                lblDesc.Visibility = Visibility.Visible;
                lblPrice.Visibility = Visibility.Visible;
                lblVeg.Visibility = Visibility.Visible;
                txtDesc.Visibility = Visibility.Visible;
                txtPrice.Visibility = Visibility.Visible;
                rdVegNo.Visibility = Visibility.Visible;
                rdVegYes.Visibility = Visibility.Visible;
                btnAddToMenu.Visibility = Visibility.Visible;

            }
            else
            {
                item.Desc = txtDesc.Text + " \t $";
                item.Price = double.Parse(txtPrice.Text);
                if (txtVeg.Text == "Y")
                {
                    item.Vegetarian = true;
                }
                else
                {
                    item.Vegetarian = false;

                }
                newRest.removeItem(lstMod.SelectedIndex);
                lstMod.ItemsSource = null;
                newRest.addItemToMenu(item);
                lstMod.ItemsSource = newRest.completeMenu();
                rdVegNo.IsChecked = false;
                rdVegYes.IsChecked = false;
                txtDesc.Clear();
                txtPrice.Clear();
                txtVeg.Clear();

                lblVeg.Visibility = Visibility.Hidden;
                txtVeg.Visibility = Visibility.Hidden;
                txtDesc.Visibility = Visibility.Hidden;
                lblDesc.Visibility = Visibility.Hidden;
                lblAmendingItem.Visibility = Visibility.Hidden;
                btnAmendMenuItem.Visibility = Visibility.Hidden;
                lblPrice.Visibility = Visibility.Hidden;
                txtPrice.Visibility = Visibility.Hidden;
                lblAddingItem.Visibility = Visibility.Hidden;
                btnAmendItem.Visibility = Visibility.Visible;
                btnAddToMenu.Visibility = Visibility.Hidden;
                rdVegNo.Visibility = Visibility.Hidden;
                rdVegYes.Visibility = Visibility.Hidden;
            }
        }



        //button used to display all the actions that can be performed on the server's list
        private void btnServers_Click(object sender, RoutedEventArgs e)
        {
            lstMod.Visibility = Visibility.Hidden;
            lstDriversMod.Visibility = Visibility.Hidden;
            lstServersMod.Visibility = Visibility.Visible;

            lstServersMod.ItemsSource = newRest.servers();

            btnAddItem.Visibility = Visibility.Hidden;
            btnAmendItem.Visibility = Visibility.Hidden;
            btnRemoveItem.Visibility = Visibility.Hidden;
            btnAddServer.Visibility = Visibility.Visible;
            btnAmendServer.Visibility = Visibility.Visible;
            btnRemoveServer.Visibility = Visibility.Visible;
            btnAddDriver.Visibility = Visibility.Hidden;
            btnAmendDriver.Visibility = Visibility.Hidden;
            btnRemoveDriver.Visibility = Visibility.Hidden;
            lblAddingItem.Visibility = Visibility.Hidden;
            lblDesc.Visibility = Visibility.Hidden;
            txtDesc.Visibility = Visibility.Hidden;
            lblPrice.Visibility = Visibility.Hidden;
            txtPrice.Visibility = Visibility.Hidden;
            lblVeg.Visibility = Visibility.Hidden;
            txtVeg.Visibility = Visibility.Hidden;
            rdVegNo.Visibility = Visibility.Hidden;
            rdVegYes.Visibility = Visibility.Hidden;
            btnAddToMenu.Visibility = Visibility.Hidden;
            lblAmendingItem.Visibility = Visibility.Hidden;
            btnAmendMenuItem.Visibility = Visibility.Hidden;
            lblAddingDriver.Visibility = Visibility.Hidden;
            lblDriverName.Visibility = Visibility.Hidden;
            txtDriverName.Visibility = Visibility.Hidden;
            lblDriverId.Visibility = Visibility.Hidden;
            txtDriverId.Visibility = Visibility.Hidden;
            lblReg.Visibility = Visibility.Hidden;
            txtReg.Visibility = Visibility.Hidden;
            btnAddToDriver.Visibility = Visibility.Hidden;
            lblAmendingDriver.Visibility = Visibility.Hidden;
            btnAddToDriver.Visibility = Visibility.Hidden;
            btnAmendToDriver.Visibility = Visibility.Hidden;


        }
        //buton to add a server
        private void btnAddServer_Click(object sender, RoutedEventArgs e)
        {
            lblAddingServer.Visibility = Visibility.Visible;
            lblAmendingServer.Visibility = Visibility.Hidden;
            lblServerName.Visibility = Visibility.Visible;
            txtServerName.Visibility = Visibility.Visible;
            lblServerId.Visibility = Visibility.Visible;
            txtServerId.Visibility = Visibility.Visible;
            btnAddToServer.Visibility = Visibility.Visible;
            lblAddingItem.Visibility = Visibility.Hidden;
            lblAmendingItem.Visibility = Visibility.Hidden;
            lblDesc.Visibility = Visibility.Hidden;
            txtDesc.Visibility = Visibility.Hidden;
            lblPrice.Visibility = Visibility.Hidden;
            txtPrice.Visibility = Visibility.Hidden;
            lblVeg.Visibility = Visibility.Hidden;
            rdVegNo.Visibility = Visibility.Hidden;
            rdVegYes.Visibility = Visibility.Hidden;
            txtVeg.Visibility = Visibility.Hidden;
            btnAddToMenu.Visibility = Visibility.Hidden;
            btnAmendMenuItem.Visibility = Visibility.Hidden;
            btnAmendToServer.Visibility = Visibility.Hidden;





        }

        //Button to add servers to list
        private void btnAddToServer_Click(object sender, RoutedEventArgs e)
        {
            Server waiting = new Server();
            waiting.EmployeeName = txtServerName.Text;
            waiting.EmployeeId = Int32.Parse(txtServerId.Text);

            lstServersMod.ItemsSource = null;
            newRest.addEmployee(waiting);
            lstServersMod.ItemsSource = newRest.servers();

            txtServerName.Clear();
            txtServerId.Clear();

            lblAddingServer.Visibility = Visibility.Hidden;
            lblServerName.Visibility = Visibility.Hidden;
            txtServerName.Visibility = Visibility.Hidden;
            lblServerId.Visibility = Visibility.Hidden;
            txtServerId.Visibility = Visibility.Hidden;
            btnAddToServer.Visibility = Visibility.Hidden;
        }

        //button used to amend the server
        private void btnAmendServer_Click(object sender, RoutedEventArgs e)
        {
            if (lstServersMod.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a server first");
            }
            else
            {
                lblAmendingServer.Visibility = Visibility.Visible;
                lblServerName.Visibility = Visibility.Visible;
                txtServerName.Visibility = Visibility.Visible;
                lblServerId.Visibility = Visibility.Visible;
                txtServerId.Visibility = Visibility.Visible;
                btnAmendToServer.Visibility = Visibility.Visible;
                btnAddToServer.Visibility = Visibility.Hidden;
                lblAddingServer.Visibility = Visibility.Hidden;


                String servAmend = lstServersMod.SelectedItem.ToString();
                string[] servsplit = servAmend.Split('\t');
                txtServerName.Text = servsplit[0];
                txtServerId.Text = servsplit[1];
            }


        }
        //button amending server from the list
        private void btnAmendToServer_Click(object sender, RoutedEventArgs e)
        {
            Server server = new Server();
            string testName = txtServerName.Text;
            string testId = txtServerId.ToString();

            if (!System.Text.RegularExpressions.Regex.IsMatch(testName, "^[a-zA-Z ]+$"))
            {
                MessageBox.Show("Please enter a valid name");
                txtServerName.Focus();
                lblAddingItem.Visibility = Visibility.Hidden;
                lblDesc.Visibility = Visibility.Hidden;
                lblPrice.Visibility = Visibility.Hidden;
                lblVeg.Visibility = Visibility.Hidden;
                txtDesc.Visibility = Visibility.Hidden;
                txtPrice.Visibility = Visibility.Hidden;
                rdVegNo.Visibility = Visibility.Hidden;
                rdVegYes.Visibility = Visibility.Hidden;
                btnAddToMenu.Visibility = Visibility.Hidden;
                txtDesc.Clear();
            }
            else if (!System.Text.RegularExpressions.Regex.IsMatch(testId, "[0-9]+$"))
            {
                MessageBox.Show("Please enter a valid id");
                txtPrice.Focus();

                lblAddingItem.Visibility = Visibility.Hidden;
                lblDesc.Visibility = Visibility.Hidden;
                lblPrice.Visibility = Visibility.Hidden;
                lblVeg.Visibility = Visibility.Hidden;
                txtDesc.Visibility = Visibility.Hidden;
                txtPrice.Visibility = Visibility.Hidden;
                rdVegNo.Visibility = Visibility.Hidden;
                rdVegYes.Visibility = Visibility.Hidden;
                btnAddToMenu.Visibility = Visibility.Hidden;
                txtPrice.Clear();
            }
            else
            {


                server.EmployeeName = txtServerName.Text;
                server.EmployeeId = Int32.Parse(txtServerId.Text);

                newRest.removeServer(lstServersMod.SelectedItem.ToString());
                lstServersMod.ItemsSource = null;

                newRest.addEmployee(server);
                lstServersMod.ItemsSource = newRest.servers();
            }

        }

        //Button to remove server from lstMod (working)
        private void btnRemoveServer_Click(object sender, RoutedEventArgs e)
        {
            newRest.removeServer(lstServersMod.SelectedItem.ToString());
            lstServersMod.ItemsSource = null;
            lstServersMod.ItemsSource = newRest.servers();
        }


        //button displaying all the action that can be performed on the list of drivers
        private void btnDrivers_Click(object sender, RoutedEventArgs e)
        {
            lstMod.Visibility = Visibility.Hidden;
            lstServersMod.Visibility = Visibility.Hidden;
            lstDriversMod.Visibility = Visibility.Visible;

            lstDriversMod.ItemsSource = newRest.drivers();
            btnAddItem.Visibility = Visibility.Hidden;
            btnAmendItem.Visibility = Visibility.Hidden;
            btnRemoveItem.Visibility = Visibility.Hidden;
            btnAddServer.Visibility = Visibility.Hidden;
            btnAmendServer.Visibility = Visibility.Hidden;
            btnRemoveServer.Visibility = Visibility.Hidden;
            btnAddDriver.Visibility = Visibility.Visible;
            btnAmendDriver.Visibility = Visibility.Visible;
            btnRemoveDriver.Visibility = Visibility.Visible;
            lblAddingItem.Visibility = Visibility.Hidden;
            lblDesc.Visibility = Visibility.Hidden;
            txtDesc.Visibility = Visibility.Hidden;
            lblPrice.Visibility = Visibility.Hidden;
            txtPrice.Visibility = Visibility.Hidden;
            lblVeg.Visibility = Visibility.Hidden;
            txtVeg.Visibility = Visibility.Hidden;
            rdVegNo.Visibility = Visibility.Hidden;
            rdVegYes.Visibility = Visibility.Hidden;
            btnAddToMenu.Visibility = Visibility.Hidden;
            lblAmendingItem.Visibility = Visibility.Hidden;
            btnAmendMenuItem.Visibility = Visibility.Hidden;
            lblAddingServer.Visibility = Visibility.Hidden;
            lblServerName.Visibility = Visibility.Hidden;
            txtServerName.Visibility = Visibility.Hidden;
            lblServerId.Visibility = Visibility.Hidden;
            txtServerId.Visibility = Visibility.Hidden;
            btnAddToServer.Visibility = Visibility.Hidden;
            btnAmendToServer.Visibility = Visibility.Hidden;
            lblAmendingServer.Visibility = Visibility.Hidden;
        }

        //button to add driver to driver's list
        private void btnAddDriver_Click(object sender, RoutedEventArgs e)
        {
            lblAddingDriver.Visibility = Visibility.Visible;
            lblAmendingDriver.Visibility = Visibility.Hidden;
            lblDriverName.Visibility = Visibility.Visible;
            txtDriverName.Visibility = Visibility.Visible;
            lblDriverId.Visibility = Visibility.Visible;
            txtDriverId.Visibility = Visibility.Visible;
            lblReg.Visibility = Visibility.Visible;
            txtReg.Visibility = Visibility.Visible;
            btnAddToDriver.Visibility = Visibility.Visible;
        }

        //Button used to amend a driver
        private void btnAmendDriver_Click(object sender, RoutedEventArgs e)
        {
            if (lstDriversMod.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a driver first");
            }
            else
            {
                lblAddingDriver.Visibility = Visibility.Hidden;
                lblAmendingDriver.Visibility = Visibility.Visible;
                lblDriverName.Visibility = Visibility.Visible;
                txtDriverName.Visibility = Visibility.Visible;
                lblDriverId.Visibility = Visibility.Visible;
                txtDriverId.Visibility = Visibility.Visible;
                lblReg.Visibility = Visibility.Visible;
                txtReg.Visibility = Visibility.Visible;
                btnAddToDriver.Visibility = Visibility.Hidden;
                btnAmendToDriver.Visibility = Visibility.Visible;

                String driveAmend = lstDriversMod.SelectedItem.ToString();
                string[] drivesplit = driveAmend.Split('\t');
                txtDriverName.Text = drivesplit[0];
                txtDriverId.Text = drivesplit[1];
                txtReg.Text = drivesplit[2];
            }
        }

        //button amending a driver
        private void btnAmendToDriver_Click(object sender, RoutedEventArgs e)
        {
            Driver driver = new Driver();
            string testName = txtDriverName.Text;
            string testId = txtDriverId.ToString();
            string testReg = txtReg.ToString();

            if (!System.Text.RegularExpressions.Regex.IsMatch(testName, "^[a-zA-Z ]+$"))
            {
                MessageBox.Show("Please enter a valid name");
                txtDriverName.Focus();
                lblAddingItem.Visibility = Visibility.Hidden;
                lblDesc.Visibility = Visibility.Hidden;
                lblPrice.Visibility = Visibility.Hidden;
                lblVeg.Visibility = Visibility.Hidden;
                txtDesc.Visibility = Visibility.Hidden;
                txtPrice.Visibility = Visibility.Hidden;
                rdVegNo.Visibility = Visibility.Hidden;
                rdVegYes.Visibility = Visibility.Hidden;
                btnAddToMenu.Visibility = Visibility.Hidden;
             
            }
            else if (!System.Text.RegularExpressions.Regex.IsMatch(testId, "[0-9]+$"))
            {
                MessageBox.Show("Please enter a valid id");
                txtDriverId.Focus();

                lblAddingItem.Visibility = Visibility.Hidden;
                lblDesc.Visibility = Visibility.Hidden;
                lblPrice.Visibility = Visibility.Hidden;
                lblVeg.Visibility = Visibility.Hidden;
                txtDesc.Visibility = Visibility.Hidden;
                txtPrice.Visibility = Visibility.Hidden;
                rdVegNo.Visibility = Visibility.Hidden;
                rdVegYes.Visibility = Visibility.Hidden;
                btnAddToMenu.Visibility = Visibility.Hidden;
                txtPrice.Clear();
            }
            else if (!System.Text.RegularExpressions.Regex.IsMatch(testReg, "[a-zA-Z0-9]+$"))
            {
                MessageBox.Show("Please enter a valid registration number");
                txtReg.Focus();

                lblAddingItem.Visibility = Visibility.Hidden;
                lblDesc.Visibility = Visibility.Hidden;
                lblPrice.Visibility = Visibility.Hidden;
                lblVeg.Visibility = Visibility.Hidden;
                txtDesc.Visibility = Visibility.Hidden;
                txtPrice.Visibility = Visibility.Hidden;
                rdVegNo.Visibility = Visibility.Hidden;
                rdVegYes.Visibility = Visibility.Hidden;
                btnAddToMenu.Visibility = Visibility.Hidden;
                txtPrice.Clear();
            }
            else
            { 


                driver.EmployeeName = txtServerName.Text;
                driver.EmployeeId = Int32.Parse(txtServerId.Text);
                driver.Registratio = txtReg.Text;

                newRest.removeServer(lstDriversMod.SelectedItem.ToString());
                lstDriversMod.ItemsSource = null;

                newRest.addEmployee(driver);
                lstDriversMod.ItemsSource = newRest.servers();
            }
        }

        //button that on click will remome a driver
        private void btnRemoveDriver_Click(object sender, RoutedEventArgs e)
        {

            newRest.removeServer(lstDriversMod.SelectedItem.ToString());
            lstMod.ItemsSource = null;
            lstDriversMod.ItemsSource = newRest.drivers();
        }

        //Button to add drivers to the drivers list
        private void btnAddToDriver_Click(object sender, RoutedEventArgs e)
        {
            Server driving = new Server();
            driving.EmployeeName = txtDriverName.Text;
            driving.EmployeeId = Int32.Parse(txtDriverId.Text);
            
        }
        //display all the delivery  orders
        private void btnDeliveryList_Click(object sender, RoutedEventArgs e)
        {
            //bug that prevents me from displaying multimple values
            lstMod.ItemsSource = null;
            lstMod.ItemsSource = newRest.displayDeliveryList();
        }

        //method displaying all sit in orders
        private void btnSitInList_Click(object sender, RoutedEventArgs e)
        {
            //bug that prevents me from displaying multimple values
            lstMod.ItemsSource = null;
            lstMod.ItemsSource = newRest.displaySitInList();
        }

        //Button that shows the orders placed by one server
        private void btnShowOrders_Click(object sender, RoutedEventArgs e)
        {
            lstMod.Items.Clear();
            lstMod.ItemsSource = null;
            lstMod.ItemsSource = newRest.orderBySever(comboEmps.SelectedItem.ToString());
        }

        //Button managing the visibilities for the listing options
        private void btnListing_Click(object sender, RoutedEventArgs e)
        {
            lstMod.ItemsSource = null;
            lstMod.Items.Clear();
            lstDriversMod.ItemsSource = null;
            lstDriversMod.Items.Clear();
            lstServersMod.ItemsSource = null;
            lstServersMod.Items.Clear();
            btnAddItem.Visibility = Visibility.Hidden;
            btnAmendItem.Visibility = Visibility.Hidden;
            btnRemoveItem.Visibility = Visibility.Hidden;
            btnServers.Visibility = Visibility.Hidden;
            btnItems.Visibility = Visibility.Hidden;
            btnDrivers.Visibility = Visibility.Hidden;
            lblSelectServer.Visibility = Visibility.Visible;
            comboEmps.Visibility = Visibility.Visible;
            btnShowOrders.Visibility = Visibility.Visible;
            btnDeliveryList.Visibility = Visibility.Visible;
            btnSitInList.Visibility = Visibility.Visible;
            btnAddServer.Visibility = Visibility.Hidden;
            btnAmendServer.Visibility = Visibility.Hidden;
            btnRemoveServer.Visibility = Visibility.Hidden;
            btnAddDriver.Visibility = Visibility.Hidden;
            btnAmendDriver.Visibility = Visibility.Hidden;
            btnRemoveDriver.Visibility = Visibility.Hidden;
            lblAddingServer.Visibility = Visibility.Hidden;
            lblServerName.Visibility = Visibility.Hidden;
            txtServerName.Visibility = Visibility.Hidden;
            lblServerId.Visibility = Visibility.Hidden;
            txtServerId.Visibility = Visibility.Hidden;
            btnAddToServer.Visibility = Visibility.Hidden;
            btnAmendToServer.Visibility = Visibility.Hidden;
            lblAmendingServer.Visibility = Visibility.Hidden;
            lblAddingDriver.Visibility = Visibility.Hidden;
            lblDriverName.Visibility = Visibility.Hidden;
            txtDriverName.Visibility = Visibility.Hidden;
            lblDriverId.Visibility = Visibility.Hidden;
            txtDriverId.Visibility = Visibility.Hidden;
            lblReg.Visibility = Visibility.Hidden;
            txtReg.Visibility = Visibility.Hidden;
            btnAddToDriver.Visibility = Visibility.Hidden;
            lblAmendingDriver.Visibility = Visibility.Hidden;
            btnAddToDriver.Visibility = Visibility.Hidden;
            btnAmendToDriver.Visibility = Visibility.Hidden;
            btnAddDriver.Visibility = Visibility.Hidden;
            btnAmendDriver.Visibility = Visibility.Hidden;
            btnRemoveDriver.Visibility = Visibility.Hidden;
            lblAddingItem.Visibility = Visibility.Hidden;
            lblDesc.Visibility = Visibility.Hidden;
            txtDesc.Visibility = Visibility.Hidden;
            lblPrice.Visibility = Visibility.Hidden;
            txtPrice.Visibility = Visibility.Hidden;
            lblVeg.Visibility = Visibility.Hidden;
            txtVeg.Visibility = Visibility.Hidden;
            rdVegNo.Visibility = Visibility.Hidden;
            rdVegYes.Visibility = Visibility.Hidden;
            btnAddToMenu.Visibility = Visibility.Hidden;
            lblAmendingItem.Visibility = Visibility.Hidden;
            btnAmendMenuItem.Visibility = Visibility.Hidden;
            lblAddingDriver.Visibility = Visibility.Hidden;
            lblDriverName.Visibility = Visibility.Hidden;
            txtDriverName.Visibility = Visibility.Hidden;
            lblDriverId.Visibility = Visibility.Hidden;
            txtDriverId.Visibility = Visibility.Hidden;
            lblReg.Visibility = Visibility.Hidden;
            txtReg.Visibility = Visibility.Hidden;
            btnAddToDriver.Visibility = Visibility.Hidden;
            lblAmendingDriver.Visibility = Visibility.Hidden;
            btnAddToDriver.Visibility = Visibility.Hidden;
            btnAmendToDriver.Visibility = Visibility.Hidden;
            txtDesc.Clear();
            txtPrice.Clear();
            txtServerId.Clear();
            txtServerName.Clear();
            txtDriverId.Clear();
            txtDriverName.Clear();
            txtReg.Clear();

        }

        //button hiding the list options and showin the items, drivers and servers options
        private void btnOptions_Click_1(object sender, RoutedEventArgs e)
        {
            lstMod.ItemsSource = null;
            lstMod.Items.Clear();
            btnServers.Visibility = Visibility.Visible;
            btnDrivers.Visibility = Visibility.Visible;
            btnItems.Visibility = Visibility.Visible;
            lblSelectServer.Visibility = Visibility.Hidden;
            comboEmps.Visibility = Visibility.Hidden;
            btnShowOrders.Visibility = Visibility.Hidden;
            btnDeliveryList.Visibility = Visibility.Hidden;
            btnSitInList.Visibility = Visibility.Hidden;

        }

        
        //botton that uses the saveItems menu to load all the objects in .xml files
        private void btnSerItem_Click(object sender, RoutedEventArgs e)
        {
            newRest.saveItems();
        }

        //button loads xml file from memory
        private void btnLoadItem_Click(object sender, RoutedEventArgs e)
        {
            newRest.load();
          
        }
    }
}
