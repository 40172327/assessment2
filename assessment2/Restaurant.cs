﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using System.Xml.Serialization;

namespace assessment2
{
    //Restaurant class added to remove calculations from the main window class
    class Restaurant
    {
        //Creation of a list of employees, they can be server or drivers
        public List<Employee> employee = new List<Employee>();
        //Creation of list contining menu items
        public List<MenuItem> menu = new List<MenuItem>();

        private List<MenuItem> orders = new List<MenuItem>();
        //List of strings containing the delivery orders
        public List<string> DeliveryOrder = new List<string>();
        //List of strings containing the sit in orders
        private List<string> SitInOrder = new List<string>();




        //instance of restaurant created
        public Restaurant()
        {
        }

        //Create a new list of servers
        public List<Server> servers()
        {
            List<Server> tempServer = new List<Server>();

            foreach (var server in employee)
            {
                if (server.GetType() == typeof(Server))
                {
                    tempServer.Add((Server)server);
                }
            }

            return tempServer;
        }

        public int CountServer()
        {
            int count = 0;
            foreach (Employee e in employee)
            {
                if (e is Server)
                {
                    count++;
                }
            }
            return count;
        }

        //method used to display the server name and employee number
        public string showServer(int i)
        {
            string name = "";
            int count = 0;
            foreach (Employee e in employee)
            {

                if (e is Server)
                {
                    if (count == i)
                    {
                        name = e.EmployeeName + "\t" + e.EmployeeId;
                    }

                    count++;

                }
            }

            return name;
        }

        //Create a new list of drivers
        public List<Driver> drivers()
        {
            List<Driver> tempDriver = new List<Driver>();

            foreach (var driver in employee)
            {
                if (driver.GetType() == typeof(Driver))
                {
                    tempDriver.Add((Driver)driver);
                }
            }
            return tempDriver;

        }

        public void addItemToMenu(MenuItem newMenuItem)
        {
            menu.Add(newMenuItem);
        }

        //add sit in order to the list sit
        public void addSitInOrders(string newSitInOrders)
        {
            SitInOrder.Add(newSitInOrders);
        }
        public List<string> displaySitInList()
        {
            return SitInOrder;
        }

        public int CountMenu()
        {
            return menu.Count();
        }

        public List<MenuItem> completeMenu()
        {
            return menu;

        }

        public string showMenu(int i)
        {
            return menu[i].Desc + menu[i].Price + "\t" + Veg(menu[i].Vegetarian);
        }

        //Method that counts the employee of Server kind

        public int CountDriver()
        {
            int count = 0;
            foreach (Employee i in employee)
            {
                if (i is Driver)
                {
                    count++;
                }
            }

            return count;
        }

        //method used to display the drivers nam and employee number
        public string showDriver(int i)
        {
            string name = "";
            int count = 0;

            foreach (Employee e in employee)
            {
                if (e is Driver)
                {
                    if (count == i)
                    {
                        name = e.EmployeeName + "\t" + e.EmployeeId;
                    }

                    count++;

                }
            }
            return name;
        }

        //Method to add employee to the employee list
        public void addEmployee(Employee newEmp)
        {
            employee.Add(newEmp);
        }

        //method used to display the boolean option for vegetarian or not vegetarian
        public static string Veg(bool isVeg)
        {
            if (isVeg) { return "Y"; }

            return "N";
        }

        //metod do add a delivery order to a delivery order list
        public void addDeliveryOrder(string s)
        {
            DeliveryOrder.Add(s);
        }

        //method to display the list of delivery orders
        public List<string> displayDeliveryList()
        {
            return DeliveryOrder;
        }

        //method to remove items from the menu
        public void removeItem(int i)
        {
            menu.RemoveAt(i);
        }

        //Remove method uses string comaprison to identify an employee  to delete
        public string removeServer(string s)
        {
            string test1 = "";
            string test2 = "";

            for (int i = 0; i < employee.Count(); i++)
            {
                test1 = employee[i].EmployeeName;
                test2 = employee[i].EmployeeId.ToString();
                if (s.Contains(test1) && s.Contains(test2))
                {
                    employee.Remove(employee[i]);

                }
            }
            return null;

        }

        //method to add sit in orders
        public void addSitInOrder(string newSitInOrder)
        {
            SitInOrder.Add(newSitInOrder);
        }

        //Method checking  for orders inserted by a specific server
        public List<string> orderBySever(string r)
        {
            List<string> testServer = new List<string>();

            for (int i = 0; i < SitInOrder.Count; i++)
            {
                if (SitInOrder[i].Contains(r))
                {
                    testServer.Add(SitInOrder[i]);
                }
            }

            for (int i = 0; i < DeliveryOrder.Count; i++)
            {
                if (DeliveryOrder[i].Contains(r))
                {
                    testServer.Add(DeliveryOrder[i]);
                }
            }

            return testServer;
        }

        //method to save all the objects into different files
        public void saveItems()
        {
            var itemsfile = new FileStream("items.xml", FileMode.Create);
            new XmlSerializer(typeof(List<MenuItem>)).Serialize(itemsfile, menu);

            var deliveryfile = new FileStream("delivery.xml", FileMode.Create);
            new XmlSerializer(typeof(List<string>)).Serialize(deliveryfile, DeliveryOrder);


            var sitinfile = new FileStream("sitin.xml", FileMode.Create);
            new XmlSerializer(typeof(List<string>)).Serialize(sitinfile, SitInOrder);

            var employeefile = new FileStream("employee.xml", FileMode.Create);
            new XmlSerializer(typeof(List<Employee>), new Type[] { typeof(List<Driver>), typeof(List<Server>) }).Serialize(employeefile, employee);

            itemsfile.Close();
            deliveryfile.Close();

            sitinfile.Close();
            employeefile.Close();

        }


        public void load()
        {
            XmlSerializer serialItem = new XmlSerializer(typeof(List<MenuItem>));
            // To read the file, create a FileStream
            FileStream itemStream =
            new FileStream("items.xml", FileMode.Open);
            // Call the Deserialize method and cast to the object type.
            menu = (List<MenuItem>)serialItem.Deserialize(itemStream);

            XmlSerializer serialDelivery = new XmlSerializer(typeof(List<string>));
            // To read the file, create a FileStream
            FileStream deliveryStream =
            new FileStream("delivery.xml", FileMode.Open);
            // Call the Deserialize method and cast to the object type.
            DeliveryOrder = (List<string>)
            serialDelivery.Deserialize(deliveryStream);

            XmlSerializer serialSitin = new XmlSerializer(typeof(List<string>));
            // To read the file, create a FileStream
            FileStream sitinStream =
            new FileStream("sitin.xml", FileMode.Open);
            // Call the Deserialize method and cast to the object type.
            SitInOrder = (List<string>)
            serialSitin.Deserialize(sitinStream);

            //XmlSerializer serialEmployee = new XmlSerializer(typeof(List<string>));
            //// To read the file, create a FileStream
            //FileStream employeeStream =
            //new FileStream("items.xml", FileMode.Open);
            //// Call the Deserialize method and cast to the object type.
            //employee = (typeof(List<string>), new Type[] { typeof(List<Driver>), typeof(List<Server>) })
            //serialEmployee.Deserialize(employeefile, employee);





            deliveryStream.Close();
            itemStream.Close();

        }





    }
}
