﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    public class Employee
    {
        public string empName;
        public int empId;

        public string EmployeeName
        {
            get { return empName; }
            set
            {
                //Validation of the EmployeeName propertie, if no value is entered an error message is
                //shown to user
                if (value.Equals(""))
                {

                    throw new ArgumentException("Please enter a valid name");
                }
                empName = value;
            }
        }

        public int EmployeeId
        {
            get { return empId; }
            set
            {
                //Validation of the EmployeeName propertie, if no value is entered an error message is
                //shown to user
                if (value.Equals(""))
                {

                    throw new ArgumentException("Please enter a valid Id");
                }
                empId = value;
            }
        }
        



    }
}
