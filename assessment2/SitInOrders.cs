﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    class SitInOrders
    {
        private int tabNo;

        //Create a list to store sit in orders
        private List<MenuItem> sitInItems = new List<MenuItem>();

        public List<MenuItem> getItem()
        {
            return sitInItems;
        }

        public void addItems(List<MenuItem> i)
        {
            sitInItems.AddRange(i);
        }


        public int TableNumber
        {
            get { return tabNo; }
            set
            {
                //Validation of the Table Number propertie, if no value is entered an error message is
                //shown to user
                if (value.Equals(""))
                {

                    throw new ArgumentException("Please enter a valid name");
                }
                tabNo = value;
            }
        }



    }
}
