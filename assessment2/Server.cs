﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    public class Server : Employee
    {
        private int tabNo;

        public int TableNo
        {
            get
            {
                return tabNo;
            }
            set
            {
                tabNo = value;
            }
        }

        public override string ToString()
        {
            return empName.ToString() + "\t " + empId;
        }
    }
}
