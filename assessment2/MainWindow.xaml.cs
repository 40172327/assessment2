﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace assessment2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Instance of Restaurant created so that we can pass method to the main
        Restaurant rest = new Restaurant();
        //New list created to store items added from the menu
        List<MenuItem> billList = new List<MenuItem>();
        
        public MainWindow()
        {
            InitializeComponent();

            //creation of 3 servers objects added to the employee list
            Server ser0 = new Server();
            ser0.EmployeeId = 1;
            ser0.EmployeeName = "Filippa";
            rest.addEmployee(ser0);
            

            Server ser1 = new Server();
            ser1.EmployeeId = 2;
            ser1.EmployeeName = "Mark";
            rest.addEmployee(ser1);

            Server ser2 = new Server();
            ser2.EmployeeId = 3;
            ser2.EmployeeName = "Dan";
            rest.addEmployee(ser2);

            Server ser3 = new Server();
            ser3.EmployeeId = 4;
            ser3.EmployeeName = "Rob";
            rest.addEmployee(ser3);


            //For loop used to populate the servers combobox
            for (int i = 0; i < rest.CountServer(); i++)
            {
                comboServer.Items.Add(rest.showServer(i));

            }

            //command used to assign index 0 of comboserver to emp0 
            comboServer.SelectedIndex = 0;

            //4 drivers objects created 
            Driver drv0 = new Driver();
            drv0.EmployeeName = "Fuzz";
            drv0.EmployeeId = 7;
            drv0.Registratio = "tre456";
            rest.addEmployee(drv0);

            Driver drv1 = new Driver();
            drv1.EmployeeName = "Slash";
            drv1.EmployeeId = 8;
            drv1.Registratio = "fgd564";
            rest.addEmployee(drv1);

            Driver drv2 = new Driver();
            drv2.EmployeeName = "Pluto";
            drv2.EmployeeId = 9;
            drv2.Registratio = "bvc654";
            rest.addEmployee(drv2);

            Driver drv3 = new Driver();
            drv3.EmployeeName = "Charles";
            drv3.EmployeeId = 6;
            drv3.Registratio = "cvb564";
            rest.addEmployee(drv3);

            //for loop adds the drivers the employee list
            for (int i = 0; i < rest.CountDriver(); i++)
            {
                comboDriver.Items.Add(rest.showDriver(i));
            }

            //command used to assign index 0 of combodriver to drv0
            comboDriver.SelectedIndex = 0;

            //creation of 4 dishes items to add to menu
            MenuItem dish0 = new MenuItem();
            dish0.Desc = "Bolognese Pasta \t $";
            dish0.Price = 20.50;
            dish0.Vegetarian = false;
            rest.addItemToMenu(dish0);

            MenuItem dish1 = new MenuItem();
            dish1.Desc = "Fishy Cake \t $";
            dish1.Price = 20.50;
            dish1.Vegetarian = true;
            rest.addItemToMenu(dish1);

            MenuItem dish2 = new MenuItem();
            dish2.Desc = "Apples Pie \t $";
            dish2.Price = 10.50;
            dish2.Vegetarian = false;
            rest.addItemToMenu(dish2);

            MenuItem dish3 = new MenuItem();
            dish3.Desc = "Fishy Cake \t $";
            dish3.Price = 20.50;
            dish3.Vegetarian = true;
            rest.addItemToMenu(dish3);

            //lstMenu is populated with the 4 dishes from the menu list
            lstMenu.ItemsSource = rest.completeMenu();
        }

        //on button click items get added to the billList
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (radioDelivery.IsChecked == false && radioSitIn.IsChecked == false)
            {
                MessageBox.Show("You must select a sit-in or delivery order");
            }
            else
            {

                listBill.ItemsSource = null;
                //add  a menu item to the bill
                billList.Add((MenuItem)lstMenu.SelectedItem);
                listBill.ItemsSource = billList;
               
            }
        }

        private void btnPrice_Click(object sender, RoutedEventArgs e)
        {
            double totalPrice = 0;
            double partialPrice = 0;
            double deliveryCharge = 0;
            const double vat = 0.15;
            lstListByItem.ItemsSource = null;
            lstListByItem.Items.Clear();



            if (radioDelivery.IsChecked == false && radioSitIn.IsChecked == false)
            {
                MessageBox.Show("You must select a sit-in or delivery order");
            }
            else
            {
                if (radioDelivery.IsChecked == true)
                {
                    string testName = txtCustName.Text;
                    string testAddress = txtCustAddress.Text;
                    if (!System.Text.RegularExpressions.Regex.IsMatch(testName, "^[a-zA-Z ]+$"))
                    {
                        MessageBox.Show("Please enter a valid customer name");
                        txtCustName.Focus();
                    }
                    else if (!System.Text.RegularExpressions.Regex.IsMatch(testAddress, "^[a-zA-Z0-9 ]+$"))
                    {
                        MessageBox.Show("Customer address must be entered");
                        txtCustAddress.Focus();
                    }
                    else
                    {

                        foreach (MenuItem menItem in billList)
                        {
                            partialPrice += menItem.Price;

                        }
                        //creating new delivery order
                        DeliveryOrders del = new DeliveryOrders();
                        //adding the items from list bill to the delivery in order
                        del.addItems((List<MenuItem>)billList);
                        ////adding a delivery order to the delivery list
                        //rest.addDeliveryOrder(del);
                        //calculation of the delivery charge
                        deliveryCharge = partialPrice * vat;
                        //calculation of the total price
                        totalPrice = partialPrice + deliveryCharge;

                        string billitems = "";
                        foreach (MenuItem menItem in billList)
                        {
                            billitems += menItem.Desc + " " + menItem.Price + "\n";

                        }

                        //Assign a string to the bill
                        string deliveryOrder = "Bill created by  " + "\t" + comboServer.SelectedItem + "\n" +
                                           "Delivery driver " + "\t" + comboDriver.SelectedItem + "\n" + "\n" +
                                           "Cust name: " + "\t" + txtCustName.Text + "\n" +
                                           "Cust address: " + "\t" + txtCustAddress.Text + "\n" + "\n" +
                                          "Bill items : \n" +
                                           billitems + "\n" +
                                        "Delivery charge :  " + "\t" + " $ " + deliveryCharge + "\n" +
                                        "Total price is :  " + "\t" + " $ " + totalPrice.ToString();
                        //call to a method adding then delivery order from the ListByItem listbox
                        lstListByItem.Items.Add(deliveryOrder);
                        //call a method to add the delivery order to a list of delivery orders as a string
                        rest.addDeliveryOrder("Server: " + comboServer.SelectedItem + "\n"+
                                                     "Driver: " + comboDriver.SelectedItem + "\n" +
                                                     "Cust name: " + txtCustName.Text + "\n"+
                                                     "Price: "+ "$" + totalPrice.ToString() + "\n");

                        //Clear name and address textboxes
                        txtCustAddress.Clear();
                        txtCustName.Clear();
                        clearing();
                    }

                }
                else
                {
                    if (txtTab.Text == "")
                    {
                        MessageBox.Show("A table number must be entered");
                    }
                    else
                    {
                        //Check that table number text box only contain numbers and not special characthers
                        string tableTest = txtTab.Text;
                        if (System.Text.RegularExpressions.Regex.IsMatch(tableTest, "^[0-9]+$"))
                        {
                            //Convert the table number textbox to an int
                            int tabNumber;
                            tabNumber = Int32.Parse(txtTab.Text);
                            //Check if table number is inside valid range
                            if (tabNumber >= 1 && tabNumber <= 10)
                            {
                                string billitems = "";
                                foreach (MenuItem d in billList)
                                {

                                    totalPrice += d.Price;
                                    billitems += d.Desc + " " + d.Price + "\n";

                                }
                                //creating new sitin order
                                SitInOrders s = new SitInOrders();
                                //adding the items from list bill to the sit in order
                                string sitinOrder = "Bill created by  " + "\t" + comboServer.SelectedItem.ToString() + "\n" +
                                                        "Table number " + "\t" + txtTab.Text + "\n" + "\n" +
                                                        "Bill items : " + "\n" +
                                                             billitems + "\n" +
                                                            "Total price is :  " + "\t" + "$ " + totalPrice.ToString();
                                s.addItems((List<MenuItem>)billList);
                                rest.addSitInOrder("Server: " + comboServer.SelectedItem + "\n" +
                                                    "Table: " + txtTab.Text + "\n" +
                                                     "Price: " + "$" + totalPrice.ToString() + "\n");
                                lstListByItem.Items.Add(sitinOrder);
                                //clear table textbox
                                txtTab.Clear();
                                listBill.ItemsSource = null;
                                listBill.Items.Clear();
                            }
                            else
                            {
                                MessageBox.Show("Enter a table number between 1 and 10");

                            }
                        }

                        //If table number is not in range warning messge is shown to user
                        else
                        {
                            MessageBox.Show("Enter a valid table number");

                        }
                        clearing();
                    }
                }
            }
        }


        //Delivery radio button method that display or hides element based on the state of the Sit In radio button
        private void radioDelivery_Checked(object sender, RoutedEventArgs e)
        {

            listBill.ItemsSource = null;
            billList.Clear();
            if (txtTab.IsVisible)
            {
                if (MessageBox.Show("Do you want to place a Delivery order? All Sit-In order details will be lost", "Exit Sit-In order?",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {

                    lblDriver.Visibility = Visibility.Visible;
                    comboDriver.Visibility = Visibility.Visible;
                    txtCustName.Visibility = Visibility.Visible;
                    lblCustName.Visibility = Visibility.Visible;
                    txtCustAddress.Visibility = Visibility.Visible;
                    lblCustAddress.Visibility = Visibility.Visible;
                    lblDelivery.Visibility = Visibility.Visible;
                    txtTab.Visibility = Visibility.Hidden;
                    lblTab.Visibility = Visibility.Hidden;
                    lblSitinOrder.Visibility = Visibility.Hidden;
                    txtTab.Clear();



                }
                else
                {
                    lblDriver.Visibility = Visibility.Hidden;
                    comboDriver.Visibility = Visibility.Hidden;
                    txtCustName.Visibility = Visibility.Hidden;
                    lblCustName.Visibility = Visibility.Hidden;
                    txtCustAddress.Visibility = Visibility.Hidden;
                    lblCustAddress.Visibility = Visibility.Hidden;
                    lblDelivery.Visibility = Visibility.Hidden;
                    txtTab.Visibility = Visibility.Visible;
                    lblTab.Visibility = Visibility.Visible;
                    lblSitinOrder.Visibility = Visibility.Visible;
                    radioDelivery.IsChecked = false;
                    radioSitIn.IsChecked = true;


                }

            }
            else
            {
                lblDriver.Visibility = Visibility.Visible;
                comboDriver.Visibility = Visibility.Visible;
                txtCustName.Visibility = Visibility.Visible;
                lblCustName.Visibility = Visibility.Visible;
                txtCustAddress.Visibility = Visibility.Visible;
                lblCustAddress.Visibility = Visibility.Visible;
                lblDelivery.Visibility = Visibility.Visible;
                txtTab.Visibility = Visibility.Hidden;
                lblTab.Visibility = Visibility.Hidden;
                lblSitinOrder.Visibility = Visibility.Hidden;

            }
        }
        //Sit In radio button method that display or hides element based on the state of the Delivery radio button
        private void radioSitIn_Checked(object sender, RoutedEventArgs e)
        {

            listBill.ItemsSource = null;
            billList.Clear();
            if (txtCustAddress.IsVisible)
            {
                if (MessageBox.Show("Do you want to place a Sit-In order? All Delivery order details will be lost", "Exit delivery order?",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    listBill.ItemsSource = null;
                    billList.Clear();
                    txtTab.Visibility = Visibility.Visible;
                    lblTab.Visibility = Visibility.Visible;
                    lblSitinOrder.Visibility = Visibility.Visible;
                    lblDriver.Visibility = Visibility.Hidden;
                    comboDriver.Visibility = Visibility.Hidden;
                    txtCustName.Visibility = Visibility.Hidden;
                    lblCustName.Visibility = Visibility.Hidden;
                    txtCustAddress.Visibility = Visibility.Hidden;
                    lblCustAddress.Visibility = Visibility.Hidden;
                    lblDelivery.Visibility = Visibility.Hidden;
                    txtCustAddress.Clear();
                    txtCustName.Clear();
                    listBill.ItemsSource = null;
                    listBill.Items.Clear();
                }
                else
                {
                    lblDriver.Visibility = Visibility.Visible;
                    comboDriver.Visibility = Visibility.Visible;
                    txtCustName.Visibility = Visibility.Visible;
                    lblCustName.Visibility = Visibility.Visible;
                    txtCustAddress.Visibility = Visibility.Visible;
                    lblCustAddress.Visibility = Visibility.Visible;
                    lblDelivery.Visibility = Visibility.Visible;
                    txtTab.Visibility = Visibility.Hidden;
                    lblTab.Visibility = Visibility.Hidden;
                    lblSitinOrder.Visibility = Visibility.Hidden;
                    radioDelivery.IsChecked = true;
                    radioSitIn.IsChecked = false;


                }
            }
            else
            {
                lblSitinOrder.Visibility = Visibility.Visible;
                txtTab.Visibility = Visibility.Visible;
                lblTab.Visibility = Visibility.Visible;
            }

        }

        //Manager button opens the manager view window
        private void btnManager_Click(object sender, RoutedEventArgs e)
        {
            ManagerWindow manWin = new ManagerWindow(rest);
            manWin.Show();
        }

        //Method used to clear the main window when creating a different order
        private void clearing()
        {
            billList.Clear();
            radioDelivery.IsChecked = false;
            radioSitIn.IsChecked = false;
            txtTab.Visibility = Visibility.Hidden;
            txtCustName.Visibility = Visibility.Hidden;
            txtCustAddress.Visibility = Visibility.Hidden;
            comboDriver.Visibility = Visibility.Hidden;
            lblTab.Visibility = Visibility.Hidden;
            lblSitinOrder.Visibility = Visibility.Hidden;
            lblDelivery.Visibility = Visibility.Hidden;
            lblCustName.Visibility = Visibility.Hidden;
            lblCustAddress.Visibility = Visibility.Hidden;
            lblDriver.Visibility = Visibility.Hidden;
        }

        

        
    }
}