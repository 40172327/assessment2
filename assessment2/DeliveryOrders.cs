﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment2
{
    class DeliveryOrders
    {
        private string custName;
        private string custAddress;

        private List<MenuItem> deliveryItems = new List<MenuItem>();


        public List<MenuItem> getItem()
        {
            return deliveryItems;
        }

        //Method that adds all the items at once 
        public void addItems(List<MenuItem> i)
        {
            deliveryItems.AddRange(i);
        }

        public string CustomerName
        {
            get { return custName; }
            set
            {
                //Validation of the CustomerName propertie, if no value is entered an error message is
                //shown to user
                if (value.Equals(""))
                {

                    throw new ArgumentException("Please enter a valid name");
                }
                custName = value;
            }
        }
        public string CustomerAddress
        {
            get { return custAddress; }
            set
            {
                //Validation of the CustomerAddress propertie, if no value is entered an error message is
                //shown to user
                if (value.Equals(""))
                {

                    throw new ArgumentException("Please enter a valid name");
                }
                custAddress = value;
            }
        }



    }
}
